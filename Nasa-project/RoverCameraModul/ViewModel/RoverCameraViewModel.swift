//
//  RoverCameraViewModel.swift
//  Nasa-project
//
//  Created by Misha on 17.11.2022.
//

import Foundation

protocol RoverCameraViewModelDelegate: AnyObject {
    func updateData()
}

final class RoverCameraViewModel {
    
    private let networkService = NetworkService()
    
    weak var delegate: RoverCameraViewModelDelegate?
    
    var photos: [Photos] = [] {
        didSet {
            DispatchQueue.main.async {
                self.delegate?.updateData()
            }
        }
    }
    
    var shortCameraName: String = " "
    
    func loadPhotos(camera: String, date: String) {
        
        switch camera {
        case "Front Hazard Avoidance Camera":
            shortCameraName = "fhaz"
        case "Rear Hazard Avoidance Camera":
            shortCameraName = "rhaz"
        case "Mast Camera":
            shortCameraName = "mast"
        case "Chemistry and Camera Complex":
            shortCameraName = "chemcam"
        case "Mars Hand Lens Imager":
            shortCameraName = "mahli"
        case "Mars Descent Imager":
            shortCameraName = "mardi"
        case "Navigation Camera":
            shortCameraName = "navcam"
        case "Panoramic Camera":
            shortCameraName = "pancam"
        case "Miniature Thermal Emission Spectrometer (Mini-TES)":
            shortCameraName = "minites"
        default:
            print("No camera")
        }
        
        networkService.getPhotos(camera: shortCameraName, date: date) { [weak self] result in
            switch result {
            case .success(let photoCollection):
                self?.photos = photoCollection
            case .failure:
                break
            }
        }
    }
    
    func tranformDate(date: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let dateDate = formatter.date(from: date)
        formatter.dateFormat = "dd MMM, YYYY"
        guard let dateDate = dateDate else { return "" }
        let dateString = formatter.string(from: dateDate)
        return dateString
    }
}
