//
//  RoverCameraView.swift
//  Nasa-project
//
//  Created by Misha on 17.11.2022.
//

import UIKit

protocol RoverCameraViewDelegate: AnyObject {
    func setItemsCount() -> Int
    func setPhotos() -> [Photos]
    func openPhoto(index: Int)
}

final class RoverCameraView: UIView {
    
    weak var roverCameraViewDelegate: RoverCameraViewDelegate?
    
    var cameraChoosed: String
    var dateChoosed: String
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(RoverCameraCell.self, forCellWithReuseIdentifier: "cell")
        return collectionView
    }()
    
    required init(cameraChoosed: String, dateChoosed: String) {
        self.cameraChoosed = cameraChoosed
        self.dateChoosed = dateChoosed
        super.init(frame: .zero)
        
        self.backgroundColor = UIColor(rgb: 0xDCCEBE)
        
        self.addSubview(collectionView)
        
        setupConstraints()
        
        setupCollectionView()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        
        collectionView.topAnchor.constraint(equalTo: self.topAnchor, constant: 50).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    private func setupCollectionView() {
        collectionView.backgroundColor = UIColor(rgb: 0xDCCEBE)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
}

extension RoverCameraView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return roverCameraViewDelegate?.setItemsCount() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, sizeForItemAt: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.5, height: collectionView.frame.height / 5.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RoverCameraCell
        let imageURLSring = roverCameraViewDelegate?.setPhotos()[indexPath.item].img_src
        cell.configure(with: imageURLSring ?? "")
        return cell
    }
}

extension RoverCameraView: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        roverCameraViewDelegate?.openPhoto(index: indexPath.item)
    }
}

extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
    
