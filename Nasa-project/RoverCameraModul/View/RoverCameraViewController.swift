//
//  RoverCameraViewController.swift
//  Nasa-project
//
//  Created by Misha on 17.11.2022.
//

import UIKit

final class RoverCameraViewController: UIViewController {
    
    var cameraChoosed: String
    var dateChoosed: String
    
    let viewModel = RoverCameraViewModel()
    
    var roverCameraView: RoverCameraView?
    
    init(cameraChoosed: String, dateChoosed: String) {
        self.cameraChoosed = cameraChoosed
        self.dateChoosed = dateChoosed
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roverCameraView = RoverCameraView(cameraChoosed: cameraChoosed, dateChoosed: dateChoosed)
        
        self.view = roverCameraView
        
        viewModel.delegate = self
        roverCameraView?.roverCameraViewDelegate = self
        
        viewModel.loadPhotos(camera: cameraChoosed, date: dateChoosed)
        
        setupNavigationBar()
        
    }
    
    private func setupNavigationBar() {
        
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(image: UIImage(named: "backIcon"), style: .plain, target: self, action: nil)
        navigationController?.navigationBar.topItem?.backBarButtonItem?.tintColor = .black
        
        let cameraLabel = UILabel()
        cameraLabel.text = roverCameraView?.cameraChoosed
        cameraLabel.font = UIFont(name: "TerminalDosis-Bold", size: 13)
        cameraLabel.textColor = .black
        
        let dateLabel = UILabel()
        dateLabel.text = viewModel.tranformDate(date: dateChoosed)
        dateLabel.font = UIFont(name: "TerminalDosis-Regular", size: 13)
        dateLabel.textColor = .black
        dateLabel.textAlignment = .center
        
        
        let stackView = UIStackView(arrangedSubviews: [cameraLabel, dateLabel])
        stackView.axis = .vertical
        stackView.frame.size = CGSize(width: cameraLabel.frame.size.width, height: max(cameraLabel.frame.size.height, dateLabel.frame.size.height))
        stackView.spacing = 10
        
        navigationItem.titleView = stackView
    }
}

extension RoverCameraViewController: RoverCameraViewModelDelegate {
    
    func updateData() {
        roverCameraView?.collectionView.reloadData()
    }
}

extension RoverCameraViewController: RoverCameraViewDelegate {
    
    func setItemsCount() -> Int {
        return viewModel.photos.count
    }
    
    func setPhotos() -> [Photos] {
        let photos = viewModel.photos
        return photos
    }
    
    func openPhoto(index: Int) {
        let photo = viewModel.photos[index]
        let detailsViewModel = DetailsViewModel()
        detailsViewModel.photo = photo
        detailsViewModel.camera = cameraChoosed
        detailsViewModel.date = dateChoosed
        let detailsViewController = DetailsViewController(viewModel: detailsViewModel)
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
    
}

