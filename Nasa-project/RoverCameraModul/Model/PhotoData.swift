//
//  PhotoData.swift
//  Nasa-project
//
//  Created by Misha on 17.11.2022.
//

struct PhotoData: Codable {
    let photos: [Photos]
}

struct Photos: Codable {
    let id: Int
    let sol: Int
    let camera: Camera
    let img_src: String
    let earth_date: String
    let rover: Rover
}

struct Camera: Codable {
    let id: Int
    let name: String
    let rover_id: Int
    let full_name: String
}

struct Rover: Codable {
    let id: Int
    let name: String
    let landing_date: String
    let launch_date: String
    let status: String
}
