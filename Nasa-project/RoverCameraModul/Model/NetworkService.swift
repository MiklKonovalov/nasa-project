//
//  NetworkService.swift
//  Nasa-project
//
//  Created by Misha on 17.11.2022.
//

import UIKit

final class NetworkService {
    
    func getPhotos(camera: String, date: String, completion: @escaping(Result<[Photos], Error>) -> Void) {
        let urlString = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=\(date)&camera=\(camera)&api_key=hhqHXyNmy8eTlk7PgYauz2lBjMR15NIkisQfxCC2"
        
        guard let url = URL(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { data, responce, error in
            guard let data = data, error == nil else { return }
            
            do {
                let jsonResult = try JSONDecoder().decode(PhotoData.self, from: data)
                completion(.success(jsonResult.photos))
            }
            catch {
                print(error)
            }
        }
        task.resume()
    }
    
}
