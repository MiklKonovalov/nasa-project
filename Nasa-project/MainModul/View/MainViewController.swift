//
//  ViewController.swift
//  Nasa-project
//
//  Created by Misha on 16.11.2022.
//

import UIKit

final class MainViewController: UIViewController {

    private let mainView = MainView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.delegate = self
        
    }

    override func loadView() {
        self.view = mainView
    }
}

extension MainViewController: MyViewDelegate {
    
    func didTapButton() {
        let roverCameraViewController = RoverCameraViewController(cameraChoosed: mainView.cameraName, dateChoosed: mainView.dateOfPhotos)
        self.navigationController?.pushViewController(roverCameraViewController, animated: true)
    }
}
