//
//  MainView.swift
//  Nasa-project
//
//  Created by Misha on 16.11.2022.
//

import UIKit
import DropDown

protocol MyViewDelegate: AnyObject {
    func didTapButton()
}

final class MainView: UIView {
    
    var cameraName: String = ""
    var dateOfPhotos: String = ""
    
    weak var delegate: MyViewDelegate?
    
    private let selectCameraAndDateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.text = "Select Camera and Date"
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let roverCameraDropDownMenu: DropDown = {
        let menu = DropDown()
        menu.dataSource = RoverCamera().cameraNamesArray
        return menu
    }()
    
    let roverCameraDropDownMenuButton: DatePhotosButtonView = {
        let view = DatePhotosButtonView(name: "Select camera", image: UIImage(systemName: "calendar") ?? UIImage())
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let selectDateView: DatePhotosButtonView = {
        let view = DatePhotosButtonView(name: "Select date", image: UIImage(systemName: "chevron.down") ?? UIImage())
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.locale = .current
        datePicker.backgroundColor = .white
        datePicker.addTarget(self, action: #selector(dateChange), for: .valueChanged)
        datePicker.preferredDatePickerStyle = .inline
        datePicker.maximumDate = Date()
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        return datePicker
    }()
    
    private let exploreButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        button.layer.cornerRadius = 10
        button.tintColor = UIColor(rgb: 0xBF2E0E)
        button.setTitle("Explore", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(didTapExploreButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private let mainImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "MainImage")
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(rgb: 0xDCCEBE)
        
        addSubview(selectCameraAndDateLabel)
        addSubview(roverCameraDropDownMenu)
        addSubview(roverCameraDropDownMenuButton)
        addSubview(selectDateView)
        addSubview(exploreButton)
        addSubview(mainImage)
        addSubview(datePicker)
        
        
        datePicker.isHidden = true
        datePicker.backgroundColor = .white
        
        let tapOnDatePicker = UITapGestureRecognizer(target: self, action: #selector(didTapDatePicker(_:)))
        selectDateView.addGestureRecognizer(tapOnDatePicker)
        
        let tapOnDropDown = UITapGestureRecognizer(target: self, action: #selector(didTapRoverCamera(_:)))
        roverCameraDropDownMenuButton.addGestureRecognizer(tapOnDropDown)
        
        setupConstraints()
        
        roverCameraDropDownMenu.anchorView = roverCameraDropDownMenuButton
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupConstraints() {
        selectCameraAndDateLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 50).isActive = true
        selectCameraAndDateLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        selectCameraAndDateLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        roverCameraDropDownMenuButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        roverCameraDropDownMenuButton.bottomAnchor.constraint(equalTo: selectDateView.topAnchor, constant: -50).isActive = true
        roverCameraDropDownMenuButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        roverCameraDropDownMenuButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        roverCameraDropDownMenuButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        
        selectDateView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        selectDateView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        selectDateView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        selectDateView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        selectDateView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        
        datePicker.topAnchor.constraint(equalTo: selectDateView.bottomAnchor).isActive = true
        datePicker.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        datePicker.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        exploreButton.topAnchor.constraint(equalTo: selectDateView.bottomAnchor, constant: 50).isActive = true
        exploreButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        exploreButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        exploreButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        exploreButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        
        mainImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        mainImage.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        mainImage.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        mainImage.topAnchor.constraint(equalTo: exploreButton.bottomAnchor, constant: 30).isActive = true
    }
     
    private func formatDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM, YYYY"
        return formatter.string(from: date)
    }
    
    private func formatSendingDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        return formatter.string(from: date)
    }
    
    @objc func didTapRoverCamera(_ tap: UITapGestureRecognizer) {
        roverCameraDropDownMenu.show()
        roverCameraDropDownMenu.selectionAction = { [weak self] index, title in
            self?.cameraName = title
            self?.roverCameraDropDownMenuButton.name = title
        }
    }
    
    @objc func didTapDatePicker(_ tap: UITapGestureRecognizer) {
        datePicker.isHidden = false
    }
    
    @objc func dateChange() {
        selectDateView.buttonLabel.text = formatDate(date: datePicker.date)
        dateOfPhotos = formatSendingDate(date: datePicker.date)
        datePicker.isHidden = true
    }
    
    @objc func didTapExploreButton() {
        delegate?.didTapButton()
    }
    
}
