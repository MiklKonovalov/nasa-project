//
//  DatePhotosButtonView.swift
//  Nasa-project
//
//  Created by Misha on 18.11.2022.
//

import Foundation
import UIKit

class DatePhotosButtonView: UIView {
    
    var name: String {
        didSet {
            buttonLabel.text = name
        }
    }
    
    let buttonLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        label.textColor = .black
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
   let imageView: UIImageView = {
        let image = UIImageView()
        image.tintColor = .black
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    required init(name: String, image: UIImage) {
        self.name = name
        super.init(frame: .zero)
        
        self.backgroundColor = .white
        
        self.addSubview(buttonLabel)
        self.addSubview(imageView)
        
        setupConstraints()
        
        buttonLabel.text = name
        imageView.image = image
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupConstraints() {
        
        buttonLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        buttonLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        
        imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        
    }
    
}
