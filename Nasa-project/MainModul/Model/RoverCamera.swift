//
//  RoverCamera.swift
//  Nasa-project
//
//  Created by Misha on 16.11.2022.
//

struct RoverCamera {
    var cameraNamesArray = [
                            "Front Hazard Avoidance Camera",
                            "Rear Hazard Avoidance Camera",
                            "Mast Camera",
                            "Chemistry and Camera Complex",
                            "Mars Hand Lens Imager",
                            "Mars Descent Imager",
                            "Navigation Camera",
                            "Panoramic Camera",
                            "Miniature Thermal Emission Spectrometer (Mini-TES)"
                            ]
}


