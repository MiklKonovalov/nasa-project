//
//  DetailsViewViewModel.swift
//  Nasa-project
//
//  Created by Misha on 18.11.2022.
//

import Foundation
import Kingfisher

protocol DetailsViewModelDelegate: AnyObject {
    func updatePhoto(url: URL)
}

final class DetailsViewModel {
    
    var camera: String = ""
    var date: String = ""
    var photo: Photos?
    
    weak var delegate: DetailsViewModelDelegate?
    
    func loadPhoto() {
        guard let urlString = photo?.img_src, let url = URL(string: urlString) else { return }
        delegate?.updatePhoto(url: url)
    }
    
    func loadPhotoId() -> String {
        guard let photoId = photo?.id else { return "" }
        return String(photoId)
    }
    
}
