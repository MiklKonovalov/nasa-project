//
//  DetailsViewController.swift
//  Nasa-project
//
//  Created by Misha on 17.11.2022.
//

import UIKit

final class DetailsViewController: UIViewController {
    
    let detailsView = DetailsView()
    
    let viewModel: DetailsViewModel
    
    init(viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.delegate = self
        viewModel.loadPhoto()
        
        setupNavigationBar()
    }
    
    override func loadView() {
        self.view = detailsView
    }
    
    private func setupNavigationBar() {
        
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(image: UIImage(named: "backIcon"), style: .plain, target: self, action: nil)
        navigationController?.navigationBar.topItem?.backBarButtonItem?.tintColor = .white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up"), style: .plain, target: self, action: #selector(sharePhoto))
        navigationItem.rightBarButtonItem?.tintColor = .white
        
        let titleLabel = UILabel()
        titleLabel.text = "Photo id"
        titleLabel.font = UIFont(name: "TerminalDosis-Regular", size: 13)
        titleLabel.textColor = .white
        
        let photoIdLabel = UILabel()
        photoIdLabel.text = viewModel.loadPhotoId()
        photoIdLabel.font = UIFont(name: "TerminalDosis-Bold", size: 13)
        photoIdLabel.textColor = .white
        photoIdLabel.textAlignment = .center
        
        
        let stackView = UIStackView(arrangedSubviews: [titleLabel, photoIdLabel])
        stackView.axis = .vertical
        stackView.frame.size = CGSize(width: titleLabel.frame.size.width, height: max(photoIdLabel.frame.size.height, photoIdLabel.frame.size.height))
        stackView.spacing = 10
        
        navigationItem.titleView = stackView
    }
    
    @objc func sharePhoto() {
        let image = [detailsView.photoImageView]
        let activityViewController = UIActivityViewController(activityItems: image, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.message, UIActivity.ActivityType.mail ]
        present(activityViewController, animated: true, completion: nil)
    }
}

extension DetailsViewController: DetailsViewModelDelegate {
    func updatePhoto(url: URL) {
        detailsView.loadPhoto(url: url)
    }
}
